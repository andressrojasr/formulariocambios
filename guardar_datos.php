<?php
// Establecer conexión con la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "solicitud_cambios_db";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar la conexión
if ($conn->connect_error) {
  die("Error al conectar con la base de datos: " . $conn->connect_error);
}

// Obtener los datos enviados por el cliente

$NOM_PRO = $_POST["proyectoInput"];
$FEC_SOL = $_POST["fechaSolicitud"];
$NOM_SOL = $_POST["nameInput"];
$APE_SOL = $_POST["apeInput"];
$COR_SOL = $_POST["email"];
$DES_CAM = $_POST["descripcion"];
$IMP_DIR = $_POST["descripcionImpacto"];
$JUS_CAM = $_POST["descripcionJustificacion"];
$ALT_CAM = $_POST["descripcionAlternativas"];
$CON_REC = $_POST["descripcionConsecuencias"];
$FEC_CAM = $_POST["fechaPlazo"];
$COM_CAM = $_POST["descripcionComentarios"];

// Realizar las operaciones necesarias para guardar los datos en la base de datos
$sql = "INSERT INTO solicitudes (proyectoInput, fechaSolicitud, nameInput, apeInput, email, descripcion, descripcionImpacto, descripcionJustificacion, descripcionAlternativas, descripcionConsecuencias, fechaPlazo, descripcionComentarios) VALUES ('$NOM_PRO', '$FEC_SOL', '$NOM_SOL', '$APE_SOL', '$COR_SOL', '$DES_CAM', '$IMP_DIR', '$JUS_CAM', '$ALT_CAM', '$CON_REC', '$FEC_CAM', '$COM_CAM')";

if ($conn->query($sql) === true) {
  $response = array("message" => "Datos guardados correctamente");
  echo json_encode($response);
} else {
  $response = array("message" => "Error al guardar los datos: " . $conn->error);
  echo json_encode($response);
}

// Cerrar la conexión
$conn->close();

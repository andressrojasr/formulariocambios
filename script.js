const mysql = require('mysql')



const conection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'solicitud_cambios_db'
})

conection.connect((err) => {
    if (err) throw err
    console.log('Conexion exitosa')
})

conection.query('SELECT * FROM solicitudes', (err, rows) => {
    if (err) throw err
    console.log('los datos de la tabla son: ')
    console.log(rows)
})
document.getElementById("envSoli").addEventListener("submit", function (event) {
    event.preventDefault(); // Evita que se envíe el formulario de manera predeterminada

    // Obtén los valores de los campos del formulario
    var NOM_PRO = document.getElementById("proyectoInput").value;
    var FEC_SOL = document.getElementById("fechaSolicitud").value;
    var NOM_SOL = document.getElementById("nameInput").value;
    var APE_SOL = document.getElementById("apeInput").value;
    var COR_SOL = document.getElementById("email").value;
    var DES_CAM = document.getElementById("descripcion").value;
    var IMP_DIR = document.getElementById("descripcionImpacto").value;
    var JUS_CAM = document.getElementById("descripcionJustificacion").value;
    var ALT_CAM = document.getElementById("descripcionAlternativas").value;
    var CON_REC = document.getElementById("descripcionConsecuencias").value;
    var FEC_CAM = document.getElementById("fechaPlazo").value;
    var COM_CAM = document.getElementById("descripcionComentarios").value;

    // Crea un objeto FormData y agrega los datos del formulario
    var formData = new FormData();
    formData.append("proyectoInput", NOM_PRO);
    formData.append("fechaSolicitud", FEC_SOL);
    formData.append("nameInput", NOM_SOL);
    formData.append("apeInput", APE_SOL);
    formData.append("email", COR_SOL);
    formData.append("descripcion", DES_CAM);
    formData.append("descripcionImpacto", IMP_DIR);
    formData.append("descripcionJustificacion", JUS_CAM);
    formData.append("descripcionAlternativas", ALT_CAM);
    formData.append("descripcionConsecuencias", CON_REC);
    formData.append("fechaPlazo", FEC_CAM);
    formData.append("descripcionComentarios", COM_CAM);

    // Envía una solicitud HTTP POST al servidor
    fetch("guardar_datos.php", {
        method: "POST",
        body: formData
    })
        .then(function (response) {
            return response.text();
        })
        .then(function (data) {
            console.log(data); // Muestra la respuesta del servidor en la consola
        })
        .catch(function (error) {
            console.error("Error:", error);
        });
});

conection.end()

